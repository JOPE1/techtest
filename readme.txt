DeploymentServiceAPI Readme

Introduction

The DeploymentServiceAPI is a Springboot application that reads in the project.json file and
produces an HTML rendered output answering the question in the test. A html file is also produced
and this can be loaded separately into a browser window.

The API runs on localhost on port 8080.

1. Running Instructions

Please extract the zip file provided into a directory of your choice. The Zip file contains:
a. Readme.txt (this file)
b. an executable JAR called DeploymentServiceApi-0.0.1-SNAPSHOT.JAR
c. the source code (under DeploymentServiceApi directory)
d. The results file called deploymentStats.html (please open in browser)

Please run the JAR file by running the following command in a terminal window:

> java -jar DeploymentServiceApi-0.0.1-SNAPSHOT.jar

After a short time, you should see that the application is running on port 8080 of
localhost.

Open up a browser of your choice and enter in the following URL:

> localhost:8080/stats

After a short wait, the results should be displayed on in the browser. In addition,
a file called deploymentStats.html will be written to the directory where the application
is running from.
