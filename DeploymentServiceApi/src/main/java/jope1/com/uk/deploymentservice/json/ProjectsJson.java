package jope1.com.uk.deploymentservice.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "projectId",
    "projectGroup",
    "environments",
    "releases"
})
public class ProjectsJson 
{
	@JsonProperty("project_id")
	private String projectId;
	
	@JsonProperty("project_group")
	private String projectGroup;
	
	@JsonProperty("environments")
	private List<EnvironmentsJson> environments;
	
	@JsonProperty("releases")
	private List<ReleasesJson> releases;

	
	// Getters and setters
	@JsonProperty("project_id")
	public String getProjectId() 
	{
		return this.projectId;
	}

	@JsonProperty("project_id")
	public void setProjectId(String projectId) 
	{
		this.projectId = projectId;
	}

	@JsonProperty("project_group")
	public String getProjectGroup() 
	{
		return this.projectGroup;
	}

	@JsonProperty("project_group")
	public void setProjectGroup(String projectGroup) 
	{
		this.projectGroup = projectGroup;
	}

	@JsonProperty("environments")
	public List<EnvironmentsJson> getEnvironments() 
	{
		return this.environments;
	}

	@JsonProperty("environments")
	public void setEnvironments(List<EnvironmentsJson> environments) 
	{
		this.environments = environments;
	}

	@JsonProperty("releases")
	public List<ReleasesJson> getReleases() 
	{
		return this.releases;
	}

	@JsonProperty("releases")
	public void setReleases(List<ReleasesJson> releases) 
	{
		this.releases = releases;
	}

}
