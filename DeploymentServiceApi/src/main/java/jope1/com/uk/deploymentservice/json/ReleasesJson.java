package jope1.com.uk.deploymentservice.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
    "version",
    "deployments"
})
public class ReleasesJson 
{
	@JsonProperty("version")
	private String version;

	@JsonProperty("deployments")
	private List<DeploymentsJson> deployments;
	
	@JsonProperty("version")
	public String getVersion() 
	{
		return version;
	}

	@JsonProperty("version")
	public void setVersion(String version) 
	{
		this.version = version;
	}

	@JsonProperty("deployments")
	public List<DeploymentsJson> getDeployments() 
	{
		return this.deployments;
	}

	@JsonProperty("deployments")
	public void setDeployments(List<DeploymentsJson> deployments) 
	{
		this.deployments = deployments;
	}

}
