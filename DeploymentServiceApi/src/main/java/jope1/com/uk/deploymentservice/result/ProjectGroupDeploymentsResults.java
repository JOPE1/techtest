package jope1.com.uk.deploymentservice.result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jope1.com.uk.deploymentservice.json.DeploymentsJson;

/**
 * Holding class of statics at the project group level. This class is updated while
 * gathering the required statistics and is also read when reporting them back out.
 * 
 * @author jonathanpearce
 *
 */
public class ProjectGroupDeploymentsResults 
{
	/**
	 * The project group name.
	 */
	private String projectGroup;
	
	/**
	 * Stores the duration of time between integration and live deployments for 
	 * each release that has been made for this project group.
	 */
	private Map<String, List<Long>> releaseDurationForGroup = new HashMap<>();
	
	/**
	 * Stores the list of all deployments made for this group, per release.
	 */
	private Map<String, List<DeploymentsJson>> deploymentsPerRelease = new HashMap<>();
	
	/**
	 * A list of all the successful deployments made by this project group.
	 */
	private List<DeploymentsJson> successfulDeployments = new ArrayList<>();
	
	/**
	 * A list of all the unsuccessful deployments made by this group.
	 */
	private List<DeploymentsJson> unsuccessfulDeployments = new ArrayList<>();

	public ProjectGroupDeploymentsResults(final String projectGroup)
	{
		this.projectGroup = projectGroup;
		this.releaseDurationForGroup.put(projectGroup, new ArrayList<Long>());
		this.deploymentsPerRelease.put(projectGroup, new ArrayList<DeploymentsJson>());
		
	}

	public String getProjectGroup() {
		return projectGroup;
	}

	public void setProjectGroup(String projectGroup) {
		this.projectGroup = projectGroup;
	}

	public List<DeploymentsJson> getSuccessfulDeployments() {
		return successfulDeployments;
	}

	public void setSuccessfulDeployments(List<DeploymentsJson> successfulDeployments) {
		this.successfulDeployments = successfulDeployments;
	}

	public List<DeploymentsJson> getUnsuccessfulDeployments() {
		return unsuccessfulDeployments;
	}

	public void setUnsuccessfulDeployments(List<DeploymentsJson> unsuccessfulDeployments) {
		this.unsuccessfulDeployments = unsuccessfulDeployments;
	}

	public Map<String, List<DeploymentsJson>> getDeploymentsPerRelease() {
		return deploymentsPerRelease;
	}

	public void setDeploymentsPerRelease(Map<String, List<DeploymentsJson>> deploymentsPerRelease) {
		this.deploymentsPerRelease = deploymentsPerRelease;
	}


	public Map<String, List<Long>> getReleaseDurationForGroup() {
		return releaseDurationForGroup;
	}

	public void setReleaseDurationForGroup(Map<String, List<Long>> releaseDurationForGroup) {
		this.releaseDurationForGroup = releaseDurationForGroup;
	}

}
