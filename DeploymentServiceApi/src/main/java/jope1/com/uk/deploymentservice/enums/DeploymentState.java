package jope1.com.uk.deploymentservice.enums;

public enum DeploymentState 
{
	Failed("Failed"),
	Success("Success"),
	Canceled("Canceled"),
	TimedOut("TimedOut"),
	Executing("Executing"),
	Queued("Queued"),
	Cancelling("Cancelling"),
    Unknown("");

	private String deploymentState;

	DeploymentState(final String deploymentState) 
	{
		this.deploymentState = deploymentState;
	}

	public String getDeploymentState() 
	{
		return this.deploymentState;
	}
}
