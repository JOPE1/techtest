package jope1.com.uk.deploymentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main startup class
 * 
 * @author jonathanpearce
 *
 */
@SpringBootApplication
public class DeploymentServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeploymentServiceApiApplication.class, args);
	}
}
