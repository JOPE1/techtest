package jope1.com.uk.deploymentservice.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "projects"
})
public class DeploymentResultsJson 
{
	@JsonProperty("")
	private List<ProjectsJson> projects;

	@JsonProperty("")
	public List<ProjectsJson> getProjects() 
	{
		return this.projects;
	}

	@JsonProperty("")
	public void setProjects(List<ProjectsJson> projects) 
	{
		this.projects = projects;
	}

}
