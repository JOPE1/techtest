package jope1.com.uk.deploymentservice.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jope1.com.uk.deploymentservice.enums.DeploymentState;
import jope1.com.uk.deploymentservice.json.DeploymentsJson;
import jope1.com.uk.deploymentservice.result.DeploymentStatistics;
import jope1.com.uk.deploymentservice.result.ProjectGroupDeploymentsResults;
import jope1.com.uk.deploymentservice.utils.DeploymentServiceUtils;

/**
 * Class encapsulating business logic in reporting the deployment statistics.
 * 
 * @author jonathanpearce
 *
 */
@Service
public class ReportStatsService 
{
	
	/**
	 * Class encapsulating the deployment statistics to be gathered.
	 */
	@Autowired
	private DeploymentStatistics deploymentStats;
	
	/**
	 * Class encapsulating some basic utilities that are required during the gathering of the statistics.
	 */
	@Autowired
	private DeploymentServiceUtils deploymentServiceUtils;
	
	/**
	 * Constructs the response to render back out of the API. Here we are dealing with a simple string response.
	 * 
	 * @return response containing the deployment statistics.
	 */
	public String constructResponse()
	{
		String response = "";
		
		response = this.addSuccessfulDeployments(response);
		response = this.addNumSuccDeploymentsPerGroup(response);
		response = this.addNumSuccDeployPerEnv(response);
		response = this.addNumSuccDeployPerYear(response);
		response = this.addMostPopDayForLiveDeploy(response);
		response = this.addAveTimeDiffForReleases(response);
		response = this.addStatsPerGroup(response);
		
		return response;
	}
	
	private String addSuccessfulDeployments(String response)
	{
		response = response + "1. Number of successful deployments: " + deploymentStats.getNumberOfSuccDeployments() + "<br>";
		
		return response;
	}
	
	private String addNumSuccDeploymentsPerGroup(String response)
	{
		response = response + "<br>" +  "2. Number of successful deployments per group: <br>";
		
		for (Map.Entry<String, Integer> entry : deploymentStats.getSuccDeployPerGroup().entrySet()) 
		{
			response = response + entry.getKey() + " : " + entry.getValue() + "<br>";
		}
		
		return response;
	}
	
	private String addNumSuccDeployPerEnv(String response)
	{
		response = response + "<br>" +  "2. Number of successful deployments per environment: <br>";
		for (Map.Entry<String, Integer> entry : deploymentStats.getSuccDeployPerEnv().entrySet()) 
		{
			response = response + entry.getKey() + " : " + entry.getValue() + "<br>";
		}
		
		return response;
	}
	
	private String addNumSuccDeployPerYear(String response)
	{
		response = response + "<br>" +  "2. Number of successful deployments per year: <br>";
		for (Map.Entry<String, Integer> entry : deploymentStats.getSuccDeployPerYear().entrySet()) 
		{
			response = response + entry.getKey() + " : " + entry.getValue() + "<br>";
		}
		
		return response;
	}
	
	private String addMostPopDayForLiveDeploy(String response)
	{
		response = response + "<br>" +  "3. The most popular day for live deployment is: "
				+ this.getMostPopularLiveDeployDay() + "<br>";
		
		return response;
	}
	
	private String addAveTimeDiffForReleases(String response)
	{
		response = response + "<br>" + "4. Average time differences for releases between integration and live per group: <br>";
		for (Map.Entry<String, String> entry : deploymentStats.getAveTimeDiffFromIntToLive().entrySet()) 
		{
			response = response + entry.getKey() + " : " + entry.getValue() + "<br>";
		}
		
		return response;
	}
	
	private String addStatsPerGroup(String response)
	{
		response = response + "<br>" + "5. Stats per Group: " + "<br>";
		for (Map.Entry<String, ProjectGroupDeploymentsResults> entry : deploymentStats.getProjectGroupDeploymentsResults().entrySet()) 
		{
			ProjectGroupDeploymentsResults projectGroupDeploymentsResults = entry.getValue();
			String projectGroup = projectGroupDeploymentsResults.getProjectGroup(); 
			
			int numberOfSuccessfulReleasesToLive = this.getNumberOfSuccessfullReleasesToLiveForGroup(projectGroup);
			int totalNumberOfReleases = this.getNumberOfReleasesForGroup(projectGroup);
			
			response = response +  "<br>" + projectGroupDeploymentsResults.getProjectGroup();
			response = response +  "<br> Number of Successful Releases to live: " + numberOfSuccessfulReleasesToLive;
			
			response = response +  "<br> Number of Unsuccessful Releases (not making it to live): " + (totalNumberOfReleases - numberOfSuccessfulReleasesToLive);
			
			int numOfDeploymentsMadeForPipeline = this.getNumberOfDeploymentsInPipelineForGrp(projectGroup);
			// Iterate through the releases and find the number of deployments involved in each release
			response = response +  "<br> Number of deployments made for this pipeline: " + numOfDeploymentsMadeForPipeline;
			
			response = response + "<br> Number of releases deployed repeatedly onto the same environment: " + this.getNumberOfRepeatDeploymentsForRelease(projectGroup) + "<br>";
		}
		
		return response;
	}
	
	private int getNumberOfReleasesForGroup(String projectGroup)
	{
		ProjectGroupDeploymentsResults projectGroupDeploymentsResults = deploymentStats.getProjectGroupDeploymentsResults().get(projectGroup);
		
		Map<String, List<DeploymentsJson>> deploymentsPerReleaseMap = projectGroupDeploymentsResults.getDeploymentsPerRelease();
		
		return deploymentsPerReleaseMap.size();	
	}
	
	
	private String getMostPopularLiveDeployDay()
	{
		String response = "There have been no live deployments.";
		
		Map<String, Integer> deployDayMap = deploymentServiceUtils.getMostPopLiveDeployDay();
		
		if(!deployDayMap.isEmpty())
		{
			for (Map.Entry<String, Integer> entry : deployDayMap.entrySet()) 
			{
				response = entry.getKey() + " with a total of " + entry.getValue() + " deployments.";
			}
		}

		return response;
	}
	
	private int getNumberOfDeploymentsInPipelineForGrp(String projectGroup)
	{
		
		int numberOfDeployments = 0;
		
		ProjectGroupDeploymentsResults projectGroupDeploymentsResults = deploymentStats.getProjectGroupDeploymentsResults().get(projectGroup);
		
		Map<String, List<DeploymentsJson>> deploymentsPerReleaseMap = projectGroupDeploymentsResults.getDeploymentsPerRelease();
		
		for (Map.Entry<String, List<DeploymentsJson>> deploymentsPerRelease : deploymentsPerReleaseMap.entrySet())
		{
			numberOfDeployments = numberOfDeployments + deploymentsPerRelease.getValue().size();	
		}

		return numberOfDeployments;
	}
	
	private int getNumberOfSuccessfullReleasesToLiveForGroup(String projectGroup)
	{
		// Add to a set to get rid of any duplicated releases.
		Set<String> successfullReleases = new HashSet<>();
		
		ProjectGroupDeploymentsResults projectGroupDeploymentsResults = deploymentStats.getProjectGroupDeploymentsResults().get(projectGroup);
		
		Map<String, List<DeploymentsJson>> deploymentsPerReleaseMap = projectGroupDeploymentsResults.getDeploymentsPerRelease();
		
		for (Map.Entry<String, List<DeploymentsJson>> deploymentsPerRelease : deploymentsPerReleaseMap.entrySet())
		{
			String releaseNumber = deploymentsPerRelease.getKey();
			
			for (DeploymentsJson deployment : deploymentsPerRelease.getValue())
			{
				if(deployment.getEnvironment().equals("Live") && DeploymentState.Success.equals(deployment.getState()))
				{
					// This is a successful live deployment
					successfullReleases.add(releaseNumber);
				}
			}
		}
		
		return successfullReleases.size();
	
	}
	
	private int getNumberOfRepeatDeploymentsForRelease(String projectGroup)
	{
		
		ProjectGroupDeploymentsResults projectGroupDeploymentsResults = deploymentStats.getProjectGroupDeploymentsResults().get(projectGroup);
		
		Map<String, List<DeploymentsJson>> deploymentsPerReleaseMap = projectGroupDeploymentsResults.getDeploymentsPerRelease();
		Set<String> releaseDeployedToSameEnv = new HashSet<>();
		
		for (Map.Entry<String, List<DeploymentsJson>> deploymentsPerRelease : deploymentsPerReleaseMap.entrySet())
		{
			Map<String, Integer> deploymentsToEnv = new HashMap<>();
			// Get the list of deployments for this release
			List<DeploymentsJson> deploymentList = deploymentsPerRelease.getValue();
			
			for(DeploymentsJson deployment : deploymentList)
			{
				
				String deploymentToEnvKey = deployment.getEnvironment();
				
				if(deploymentsToEnv.containsKey(deploymentToEnvKey))
				{
					deploymentsToEnv.put(deploymentToEnvKey, deploymentsToEnv.get(deploymentToEnvKey)+1);
				}
				else
				{
					deploymentsToEnv.put(deploymentToEnvKey, 1);
				}
			}
			
			if(!deploymentsToEnv.isEmpty())
			{
				for (Map.Entry<String, Integer> deploymentsToEnvMap : deploymentsToEnv.entrySet())
				{
					if(deploymentsToEnvMap.getValue()>1)
					{
						releaseDeployedToSameEnv.add(deploymentsPerRelease.getKey());
						break;
					}

				}
			}
	
		}

		return releaseDeployedToSameEnv.size();
	}

}
