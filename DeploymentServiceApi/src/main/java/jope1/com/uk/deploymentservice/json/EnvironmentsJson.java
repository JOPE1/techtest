package jope1.com.uk.deploymentservice.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "environment"
})
public class EnvironmentsJson 
{
	@JsonProperty("environment")
	private String environment;

	@JsonProperty("environment")
	public String getEnvironment() 
	{
		return this.environment;
	}

	@JsonProperty("environment")
	public void setEnvironment(String environment) 
	{
		this.environment = environment;
	}

}
