package jope1.com.uk.deploymentservice.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jope1.com.uk.deploymentservice.json.DeploymentResultsJson;

/**
 * Class encapsulating all JSON business logic.
 * 
 * @author jonathanpearce
 *
 */
@Service
public class JsonService
{
	@Autowired
	private LoggingService loggingService;
	
	@Autowired
	private ResourceLoader resourceLoader;

	/**
	 * Read the JSON file from the classpath.
	 * 
	 * @return
	 */
	public DeploymentResultsJson readJsonFile()
	{
		loggingService.logInfo("Reading JSON file...");

		final ObjectMapper mapper = new ObjectMapper();
		DeploymentResultsJson deploymentResultsJson = null;
		
		loggingService.logDebug("Loading JSON file...");

		InputStream projectJson = null;
		Resource res = null;

		try 
		{
			res = resourceLoader.getResource("classpath:projects.json");
            projectJson = res.getInputStream();
			
			loggingService.logDebug("Parsing JSON file...");
			deploymentResultsJson = mapper.readValue(projectJson, DeploymentResultsJson.class);
			loggingService.logDebug("JSON file successfully loaded.");
		} 
		catch (JsonParseException e) 
		{
			loggingService.logError(e.getMessage());
		} 
		catch (JsonMappingException e) 
		{
			loggingService.logError(e.getMessage());
		} 
		catch (IOException e) 
		{
			loggingService.logError(e.getMessage());
		}
		
		return deploymentResultsJson;	

	}

}
