package jope1.com.uk.deploymentservice.json;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import jope1.com.uk.deploymentservice.enums.DeploymentState;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "environment",
    "created",
    "state",
    "name"
})
public class DeploymentsJson 
{
	@JsonProperty("environment")
	private String environment;
	
	@JsonProperty("created")
	private String created;
	
	@JsonProperty("state")
	private DeploymentState state;
	
	@JsonProperty("name")
	private String name;

	@JsonProperty("environment")
	public String getEnvironment() 
	{
		return this.environment;
	}

	@JsonProperty("environment")
	public void setEnvironment(String environment) 
	{
		this.environment = environment;
	}

	@JsonProperty("created")
	public String getCreated() 
	{
		return this.created;
	}

	@JsonProperty("created")
	public void setCreated(String created) 
	{
		this.created = created;
	}

	@JsonProperty("state")
	public DeploymentState getState() 
	{
		return this.state;
	}

	@JsonProperty("state")
	public void setState(DeploymentState state) 
	{
		this.state = state;
	}

	@JsonProperty("name")
	public String getName() 
	{
		return this.name;
	}

	@JsonProperty("name")
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public LocalDateTime getCreatedDate()
	{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		return LocalDateTime.parse(this.created, formatter);
	}

}
