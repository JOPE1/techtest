package jope1.com.uk.deploymentservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jope1.com.uk.deploymentservice.service.GatherStatsService;
import jope1.com.uk.deploymentservice.service.ReportStatsService;

/**
 * Controller class to define the end points of the API.
 * 
 * @author jonathanpearce
 *
 */
@RestController
public class DeploymentServiceController 
{
	/**
	 * Variable for application version - reads from application.yaml file or environment variable.
	 */
	@Value("${apiversion}")
	private String apiVersion;
	
	/**
	 * DeploymentService where all business logic resides.
	 */
	@Autowired
	private GatherStatsService gatherStatsService;
	
	@Autowired
	private ReportStatsService reportStatsService;
	
	/**
	 * Simple home page for the deployment service, reporting back the version number of the API.
	 * 
	 * @return simple string to say that we've reached the home page.
	 */
	@RequestMapping(value="/")
	public String home()
	{
		return "This version " + this.apiVersion + " of the Deployment Service API.";
	}
	
	/**
	 * End point to return the deployment statistics to the browser (and write to file).
	 * 
	 * @return simple HTML string response.
	 */
	@RequestMapping(value="/stats")
	public String stats()
	{
		// Call the service method to collate the deployment statistics required for the response.
		gatherStatsService.getDeploymentStats();
		
		// Ask the service to construct the response for us.
		String response = reportStatsService.constructResponse();
		
		// Write the response to a file.
		gatherStatsService.write(response);

		// Render the response on the browser.
		return response;
	}
}
