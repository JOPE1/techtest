package jope1.com.uk.deploymentservice.service;

import org.springframework.stereotype.Service;

/**
 * Class to handle logging throughout the application.
 * 
 * @author jonathanpearce
 *
 */
@Service
public class LoggingService 
{

	public void logInfo(final String message)
	{
		log("INFO: " + message);
	}
	
	public void logDebug(final String message)
	{
		log("DEBUG: " + message);
	}
	
	public void logTrace(final String message)
	{
		log("TRACE: " + message);
	}
	
	public void logWarning(final String message)
	{
		log("WARNING: " + message);
	}
	
	public void logError(final String message)
	{
		log("Error: " + message);
	}
	
	private void log(final String message)
	{
		System.out.println(message);
	}
	
}
