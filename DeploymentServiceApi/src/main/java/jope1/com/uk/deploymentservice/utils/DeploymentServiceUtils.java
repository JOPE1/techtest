package jope1.com.uk.deploymentservice.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jope1.com.uk.deploymentservice.result.DeploymentStatistics;

@Service
public class DeploymentServiceUtils 
{
	@Autowired
	private DeploymentStatistics deploymentResults;
	
	public Map<String, Integer> getMostPopLiveDeployDay()
	{
		Map<String, Integer> mostPopDayMap = new HashMap<>();
		
		Integer deploymentNumber = 0;
		String mostPopularDays = "";
		for (Map.Entry<String, Integer> entry : deploymentResults.getLiveDeploymentsPerDay().entrySet()) 
		{
			if (entry.getValue()>0 && entry.getValue()>=deploymentNumber)
			{
				deploymentNumber = entry.getValue();
				
				if(mostPopularDays=="")
				{
					mostPopularDays = entry.getKey();
				}
				else
				{
					mostPopularDays = mostPopularDays + ", " + entry.getKey();
				}
			}
		}
		
		if(mostPopularDays!="")
		{
			mostPopDayMap.put(mostPopularDays, deploymentNumber);
		}
		
		return mostPopDayMap;
		
	}

}
