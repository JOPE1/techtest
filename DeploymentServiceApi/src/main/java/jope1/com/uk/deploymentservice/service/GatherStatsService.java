package jope1.com.uk.deploymentservice.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jope1.com.uk.deploymentservice.enums.DeploymentState;
import jope1.com.uk.deploymentservice.json.DeploymentResultsJson;
import jope1.com.uk.deploymentservice.json.DeploymentsJson;
import jope1.com.uk.deploymentservice.json.EnvironmentsJson;
import jope1.com.uk.deploymentservice.json.ProjectsJson;
import jope1.com.uk.deploymentservice.json.ReleasesJson;
import jope1.com.uk.deploymentservice.result.DeploymentStatistics;
import jope1.com.uk.deploymentservice.result.ProjectGroupDeploymentsResults;

/**
 * Class encapsulating business logic in gathering the deployment statistics.
 * 
 * @author jonathanpearce
 *
 */
@Service
public class GatherStatsService 
{
	/**
	 * Class encapsulating the deployment statistics to be gathered.
	 */
	@Autowired
	private DeploymentStatistics deploymentStats;

	/**
	 * Service to deal with Json related actions.
	 */
	@Autowired
	private JsonService jsonService;
	
	/**
	 * Service to handle logging.
	 */
	@Autowired
	private LoggingService loggingService;
	
	/**
	 * The driving JSON object containing the set of JSON objects that the project.json file is
	 * read into.
	 */
	private DeploymentResultsJson deploymentResultsJson;
	
	/**
	 * Method to gather up all of the statistics required from the JSON objects.
	 * 
	 * @return {@link DeploymentStatistics}
	 */
	public DeploymentStatistics getDeploymentStats()
	{		
		this.initialiseDeploymentResultsJson();
		
		this.gatherProjectGroupStats();
		
		// Now iterate through each project group
		for (Map.Entry<String, List<ReleasesJson>> projectGroupEntry : deploymentStats.getReleasesPerGroupMap().entrySet()) 
		{ 
			// If we do not yet have this project group in the map, add it in with an initial successful
			// deployment count of 0.
			String projectGroup = projectGroupEntry.getKey();
			ProjectGroupDeploymentsResults projectGroupDeploymentsResults = deploymentStats.getProjectGroupDeploymentsResults().get(projectGroup);
				
			if(!deploymentStats.getSuccDeployPerGroup().containsKey(projectGroup))
			{
				deploymentStats.getSuccDeployPerGroup().put(projectGroup, 0);
			}
				
			for (ReleasesJson releasesJson : projectGroupEntry.getValue())
			{	
				// Add this deployment to our release map for this project group
				final String releaseVersion = releasesJson.getVersion();
				projectGroupDeploymentsResults.getDeploymentsPerRelease().put(releaseVersion, getDeployments(releasesJson));
			}
			
			this.gatherReleaseStatsForGroup(projectGroup,
					                        projectGroupDeploymentsResults);

			this.calculateAveDurationOfDeploymentsForLive(projectGroupDeploymentsResults);

		}

		return deploymentStats;
	}
	
	/**
	 * Writes the response to a file.
	 * 
	 * @param response the response to write to the file.
	 */
	public void write(String response)
	{
		File file = new File("./deploymentStats.html");
		try {
			Files.writeString(file.toPath(), response);
		} catch (IOException e1) {
			loggingService.logError(e1.getMessage());
		}
		try {
			file.createNewFile();
		} catch (IOException e) {
			loggingService.logError(e.getMessage());
		}
	}
	
	/**
	 * Reads in the projects.json file if not already done so.
	 */
	private void initialiseDeploymentResultsJson()
	{
		// Load in the JSON file, if we haven't done so already
		if(deploymentResultsJson==null)
		{
			deploymentResultsJson = jsonService.readJsonFile();
		}
	}
	
	/**
	 * Gathers all of the stats for the project group into temp holding objects to be reported upon later.
	 */
	private void gatherProjectGroupStats()
	{
		for (ProjectsJson projectsJson : getProjects(deploymentResultsJson))
		{
			final String projectGroup = projectsJson.getProjectGroup();

			// Add group into release duration for group map if it has not already been added.
			if(!deploymentStats.getProjectGroupDeploymentsResults().containsKey(projectGroup))
			{
				loggingService.logInfo("Adding Project Group: " + projectGroup);
				ProjectGroupDeploymentsResults projectGroupDeploymentsResults = new ProjectGroupDeploymentsResults(projectGroup);
				deploymentStats.getProjectGroupDeploymentsResults().put(projectGroup, projectGroupDeploymentsResults);
				deploymentStats.getReleasesPerGroupMap().put(projectGroup, new ArrayList<ReleasesJson>());
				deploymentStats.getEnvironmentsPerGroupMap().put(projectGroup, new ArrayList<EnvironmentsJson>());
			}
			
			for (ReleasesJson releasesJson : getReleases(projectsJson))
			{	
				loggingService.logInfo("Adding release " + releasesJson.getVersion());
				deploymentStats.getReleasesPerGroupMap().get(projectsJson.getProjectGroup()).add(releasesJson);
			}
			
			for (EnvironmentsJson environmentsJson : getEnvironments(projectsJson))
			{
				deploymentStats.getEnvironmentsPerGroupMap().get(projectsJson.getProjectGroup()).add(environmentsJson);
				
				// If we do not yet have this environment in the map, add it in with an initial successful
				// deployment count of 0.
				String environment = environmentsJson.getEnvironment();
				if(!deploymentStats.getSuccDeployPerEnv().containsKey(environment))
				{
					deploymentStats.getSuccDeployPerEnv().put(environment, 0);
				}
			}
			
		}
	}
	
	/**
	 * Gathers release stats for a project group and stores into temp objects to be reported upon later.
	 * 
	 * @param projectGroup the project group to gather the release statistics for.
	 * @param projectGroupDeploymentsResults the temp holding object to store the stats.
	 */
	private void gatherReleaseStatsForGroup(String projectGroup,
			                                ProjectGroupDeploymentsResults projectGroupDeploymentsResults)
	{
		for (Map.Entry<String, List<DeploymentsJson>> deploymentsPerRelease : projectGroupDeploymentsResults.getDeploymentsPerRelease().entrySet()) 
		{
			LocalDateTime succReleaseIntegrationDate = null;
			LocalDateTime succReleaseLiveDate = null;
			
		    // Iterate through deployments in the release
			for (DeploymentsJson deploymentsJson : deploymentsPerRelease.getValue())
			{
				if(deploymentsJson.getEnvironment().equals("Integration") && DeploymentState.Success==deploymentsJson.getState())
				{
					// If successful integration deployment, we can say the release reached the integration stage.
					succReleaseIntegrationDate = deploymentsJson.getCreatedDate();
				}
				
				// Live deployment stats per day
				if(deploymentsJson.getEnvironment().equals("Live"))
				{
					// If successful live deployment, we can say that the release reached the live stage.
					if(DeploymentState.Success==deploymentsJson.getState())
					{
						succReleaseLiveDate = deploymentsJson.getCreatedDate();
						// Add this to the successful release to live count.
						projectGroupDeploymentsResults.getSuccessfulDeployments().add(deploymentsJson);
					}
					
					String day = deploymentsJson.getCreatedDate().getDayOfWeek().toString();
					if(!deploymentStats.getLiveDeploymentsPerDay().containsKey(day))
					{
						deploymentStats.getLiveDeploymentsPerDay().put(day, 0);
					}
					else
					{
						deploymentStats.getLiveDeploymentsPerDay().put(day, deploymentStats.getLiveDeploymentsPerDay().get(day)+1);
					}
				}
				
				
				if(DeploymentState.Success==deploymentsJson.getState())
				{
					deploymentStats.incrementSuccessfulDeployments();
					
					// Increment count in map for this group
					deploymentStats.getSuccDeployPerGroup().put(projectGroup, deploymentStats.getSuccDeployPerGroup().get(projectGroup)+1);
				
					// Increment count in map for the environment
					String environment = deploymentsJson.getEnvironment();
					if(!deploymentStats.getSuccDeployPerEnv().containsKey(environment))
					{
						deploymentStats.getSuccDeployPerEnv().put(environment, 0);
					}
					else
					{
						deploymentStats.getSuccDeployPerEnv().put(environment, deploymentStats.getSuccDeployPerEnv().get(environment)+1);
					}
					
					// Increment counts per year
					String year = String.valueOf(deploymentsJson.getCreatedDate().getYear());
					if(!deploymentStats.getSuccDeployPerYear().containsKey(year))
					{
						deploymentStats.getSuccDeployPerYear().put(year, 0);
					}
					else
					{
						deploymentStats.getSuccDeployPerYear().put(year, deploymentStats.getSuccDeployPerYear().get(year)+1);
					}
				}
				
				// If the release has an integration and live creation date, record the time difference in a map
				if(succReleaseIntegrationDate!=null && succReleaseLiveDate!=null)
				{
					Duration duration = Duration.between(succReleaseLiveDate, succReleaseIntegrationDate);
					projectGroupDeploymentsResults.getReleaseDurationForGroup().get(projectGroup).add(duration.getSeconds());
				}
			}
	
		}
	}
	
	/**
	 * Calculates the averages time duration for a successful release in a project group.
	 * 
	 * @param projectGroupDeploymentsResults
	 */
	private void calculateAveDurationOfDeploymentsForLive(ProjectGroupDeploymentsResults projectGroupDeploymentsResults)
	{
		for (Map.Entry<String, List<Long>> entry : projectGroupDeploymentsResults.getReleaseDurationForGroup().entrySet()) 
		{
			Long aveReleaseTimeDiff = 0L;
			Long nTotalTimeDiff = 0L;
			
			if(!entry.getValue().isEmpty())
			{
				for(Long timeDiff : entry.getValue())
				{
					nTotalTimeDiff = nTotalTimeDiff + timeDiff;
				}
			
				aveReleaseTimeDiff = nTotalTimeDiff / entry.getValue().size();
				
				Duration aveTimeDiffDur = Duration.ofSeconds(aveReleaseTimeDiff);

				deploymentStats.getAveTimeDiffFromIntToLive().put(entry.getKey(), aveTimeDiffDur.toString());
		
			}
			
		}
	}	

	/**
	 * Returns the project group level of the JSON.
	 * 
	 * @param deploymentResultsJson
	 * @return
	 */
	private List<ProjectsJson> getProjects(final DeploymentResultsJson deploymentResultsJson)
	{
		return 	deploymentResultsJson.getProjects();
	}
	
	/**
	 * Returns the release level of the JSON for the project group.
	 * 
	 * @param projectsJson
	 * @return
	 */
	private List<ReleasesJson> getReleases(final ProjectsJson projectsJson)
	{
		return 	projectsJson.getReleases();
	}
	
	/**
	 * Returns the environment level of the JSON for the project group.
	 * 
	 * @param projectsJson
	 * @return
	 */
	private List<EnvironmentsJson> getEnvironments(final ProjectsJson projectsJson)
	{
		return 	projectsJson.getEnvironments();
	}
	
	/**
	 * Returns the Deployment level of the JSON for the project group.
	 * 
	 * @param releasesJson
	 * @return
	 */
	private List<DeploymentsJson> getDeployments(final ReleasesJson releasesJson)
	{
		return 	releasesJson.getDeployments();
	}

}
