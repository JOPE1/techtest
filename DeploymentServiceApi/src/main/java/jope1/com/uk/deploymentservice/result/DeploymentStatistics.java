package jope1.com.uk.deploymentservice.result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import jope1.com.uk.deploymentservice.json.EnvironmentsJson;
import jope1.com.uk.deploymentservice.json.ReleasesJson;

/**
 * Holding class to store required stats.
 * 
 * @author jonathanpearce
 *
 */
@Service
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DeploymentStatistics 
{
	private int numberOfSuccDeployments = 0;
	
	private Map<String, Integer> succDeployPerGroup = new HashMap<>();
	
	private Map<String, Integer> succDeployPerEnv = new HashMap<>();
	
	private Map<String, Integer> succDeployPerYear = new HashMap<>();
	
	private Map<String, Integer> liveDeploymentsPerDay = new HashMap<>();
	
	private Map<String, String> aveTimeDiffFromIntToLive = new HashMap<>();

	private Map<String, List<ReleasesJson>> releasesPerGroupMap = new HashMap<>();
	
	private Map<String, List<EnvironmentsJson>> environmentsPerGroupMap = new HashMap<>();
	
	private Map<String, ProjectGroupDeploymentsResults> projectGroupDeploymentsResults = new HashMap<>();
	

	public void incrementSuccessfulDeployments()
	{
		this.numberOfSuccDeployments++;
	}
	
	// Getters and setters
	public int getNumberOfSuccDeployments() {
		return numberOfSuccDeployments;
	}

	public void setNumberOfSuccDeployments(int numberOfSuccDeployments) {
		this.numberOfSuccDeployments = numberOfSuccDeployments;
	}

	public Map<String, Integer> getSuccDeployPerGroup() {
		return succDeployPerGroup;
	}

	public void setSuccDeployPerGroup(Map<String, Integer> succDeployPerGroup) {
		this.succDeployPerGroup = succDeployPerGroup;
	}
	
	public Map<String, Integer> getSuccDeployPerEnv() {
		return succDeployPerEnv;
	}


	public void setSuccDeployPerEnv(Map<String, Integer> succDeployPerEnv) {
		this.succDeployPerEnv = succDeployPerEnv;
	}


	public Map<String, Integer> getSuccDeployPerYear() {
		return succDeployPerYear;
	}


	public void setSuccDeployPerYear(Map<String, Integer> succDeployPerYear) {
		this.succDeployPerYear = succDeployPerYear;
	}
	

	public Map<String, Integer> getLiveDeploymentsPerDay() {
		return liveDeploymentsPerDay;
	}


	public void setLiveDeploymentsPerDay(Map<String, Integer> liveDeploymentsPerDay) {
		this.liveDeploymentsPerDay = liveDeploymentsPerDay;
	}
	

	public Map<String, String> getAveTimeDiffFromIntToLive() {
		return aveTimeDiffFromIntToLive;
	}


	public void setAveTimeDiffFromIntToLive(Map<String, String> aveTimeDiffFromIntToLive) {
		this.aveTimeDiffFromIntToLive = aveTimeDiffFromIntToLive;
	}


	public Map<String, List<ReleasesJson>> getReleasesPerGroupMap() {
		return releasesPerGroupMap;
	}


	public void setReleasesPerGroupMap(Map<String, List<ReleasesJson>> releasesPerGroupMap) {
		this.releasesPerGroupMap = releasesPerGroupMap;
	}


	public Map<String, List<EnvironmentsJson>> getEnvironmentsPerGroupMap() {
		return environmentsPerGroupMap;
	}


	public void setEnvironmentsPerGroupMap(Map<String, List<EnvironmentsJson>> environmentsPerGroupMap) {
		this.environmentsPerGroupMap = environmentsPerGroupMap;
	}


	public Map<String, ProjectGroupDeploymentsResults> getProjectGroupDeploymentsResults() {
		return projectGroupDeploymentsResults;
	}


	public void setProjectGroupDeploymentsResults(
			Map<String, ProjectGroupDeploymentsResults> projectGroupDeploymentsResults) {
		this.projectGroupDeploymentsResults = projectGroupDeploymentsResults;
	}

}
